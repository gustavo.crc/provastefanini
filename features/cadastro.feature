#language: pt

Funcionalidade: Cadastro
            Eu como um usuario do sistema
    Quero fazer o cadastro de um novo usuario
    Para que seja possivel armazenar e gerenciar os seus dados

    Cenario: Realizar Cadastro

        Dado que acesso a pagina de cadastro
        Quando submeto o seguinte formulario de cadastro:
            | nome             | email                 | senha       |
            | Gustavo Carranca | gustavo@stefanini.com | naotemsenha |
        Então devo ver o usuario cadastrado na pagina

    Cenario: Excluir um Usuario
        Dado que acesso a pagina de cadastro
        Quando submeto o seguinte formulario de cadastro:
            | nome          | email                 | senha       |
            | Gabriel Souza | gabriel@stefanini.com | naotemsenha |
            E devo ver o usuario cadastrado na pagina
            E clico na ação excluir
        Então valido se o usuario foi excluido da tabela


    Esquema do Cenario: Tentativa de Cadastro

        Dado que acesso a pagina de cadastro
        Quando submeto o seguinte formulario de cadastro:
            | nome         | email         | senha         |
            | <nome_input> | <email_input> | <senha_input> |
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | nome_input       | email_input           | senha_input | mensagem_output                            |
            |                  | gustavo@stefanini.com | naotemsenha | O campo Nome é obrigatório.                |
            | Gustavo Carranca |                       | naotemsenha | O campo E-mail é obrigatório.              |
            | Gustavo Carranca | gustavo@stefanini.com |             | O campo Senha é obrigatório.               |
            | Gustavo          | gustavo@stefanini.com | naotemsenha | Por favor, insira um nome completo.        |
            | Gustavo Carranca | gustavo#stefanini.com | naotemsenha | Por favor, insira um e-mail válido.        |
            | Gustavo Carranca | gustavo@stefanini.com | naotem      | A senha deve conter ao menos 8 caracteres. |
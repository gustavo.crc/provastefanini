
Before do
    @cadastro_page = CadastroPage.new
end

After do
    shot = page.save_screenshot("/logs/screenshot.png")

    Allure.add_attachment(  # Modulo do Allure pra fazer o anexo das evidencias
        name: "Screenshot",
        type: Allure::ContentType::PNG,
        source: File.open(shot)
    )
end 
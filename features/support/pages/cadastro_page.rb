
class CadastroPage
    include Capybara::DSL
    
    def open
        visit "/"
    end

    def create(user)
        find("#name").set user[:nome]
        find("#email").set user[:email]
        find("#password").set user[:senha]
            
        click_button "Cadastrar"  
    end

    def delete
        find("#removeUser1").click
    end

    def has_no_user?(email)
        return page.has_no_css?("#tdUserEmail1", text: email)
    end

    def error
        return find(".error").text
    end
end
Dado('que acesso a pagina de cadastro') do                                               
    @cadastro_page.open       
end   

Quando('submeto o seguinte formulario de cadastro:') do |table|
    @user = table.hashes[0]
    @cadastro_page.create(@user)               
end                                                                                                                                                                                                                
                                                                                           
Então('devo ver o usuario cadastrado na pagina') do
    user_table = find("#tdUserEmail1")
    expect(user_table).to have_content @user[:email]
end 

Quando('clico na ação excluir') do
    @cadastro_page.delete
end
  
Então('valido se o usuario foi excluido da tabela') do
    expect(@cadastro_page.has_no_user?([:email])).to be true
end
  
Então('vejo a mensagem de alerta: {string}') do |expect_alert|
    expect(@cadastro_page.error).to have_content expect_alert
  end